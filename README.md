---
gitea: none
include_toc: true
---

# Yubikey on Linux

A howto for using the Yubikey (5 NFC) with Linux and Android.

<img src="./U2F_USB-Token.jpg" width="400">

## Hopes/Plans/Wishes

- [x] use with TOTP (website login second factor)
- [x] use for ssh auth with FIDO2
- [x] use for website login with Webauthn (typically as a second factor)
- [x] secure a Keepass database with a key (possible but not usable at the moment due to missing compatibility with mobile client)
- [x] have means to remove data/reset the data for each of the above item
- [x] ability to create/keep a backup key - needs to be done for each service/key

## Works

### TOPT

TOTPs are stored on the YubiKey, an app on mobile acts as means to display the
OTP codes:

- Use and Android app, for example Yubico Authenticator [link to
  F-droid](https://f-droid.org/en/packages/com.yubico.yubioath/). 
- Press the `+` button to add a new token either manual or via QR code. 
- After adding the token data, press Save. 
- Place YubiKey to the phones NFC and the token will be stored to the YubiKey. 
- To remove a token, open the app, use NFC to read the data, long press an item
  and press the Delete icon.

### Webauthn/Hardware based 2FA

#### List of known sites that support 2FA

[https://2fa.directory/](https://2fa.directory/)

#### Codeberg

Codeberg (Gitea) uses Webauthn as a second factor. You can use it alongside the
existing 2FA (TOTP). To use:

- Plug the YubiKey into USB.
- Log into Codeberg via [supported web
  browser](https://caniuse.com/?search=webauthn)
- Go to [user settings](https://codeberg.org/user/settings),
  [security](https://codeberg.org/user/settings/security), Security key
- Enter `Nickname` for the new key to be added
- Press `Add Security Key`
- Touch the button on YubiKey
- Done

To remove the key, use the `Remove` button in the Codeberg → Settings →
Security (on the key you like to remove)

### KeePassXC

You can ADD a YubiKey as additional protection to the database but you cannot
([discussion on
KeePassXC](https://github.com/keepassxreboot/keepassxc/discussions/3597)) have
password OR the YubiKey. This means that the database can only be open in
places which do support the same mechanism. At the time of writing, this is not
possible with KeePassDX, [link to F-Droid](https://f-droid.org/packages/com.kunzisoft.keepass.libre/)

To add YubiKey as a secure key, you must first configure/set it up for
HMAC-SHA1 Challenge response. Here is [how to do it as per KeePassXC
documentation](https://keepassxc.org/docs/KeePassXC_UserGuide.html#_creating_a_yubikey_backup)


- Create a 20 byte HMAC key: `dd status=none if=/dev/random bs=20 count=1 | xxd
  -p -c 40`
-  Write the HMAC key to slot 2 (Set through the first switch. Out of the box
   the YubiKey OTP resides in slot 1): `ykpersonalize -2 -a -ochal-resp
   -ochal-hmac -ohmac-lt64 -oserial-api-visible -oallow-update`
- You will be asked to enter the HMAC key you created earlier, copy/paste they
  key output in the first step. Repeat step 2 for your second YubiKey using the
  same HMAC key from before. We recommend storing your HMAC key in a safe place
  (e.g., printed on paper) in case you need to recreate another key.
- Not sure how to remove, but can be overwritten by another key if needed.


### SSH FIDO2 based key encryption

SSH server and client must be > 8.2. YubiKey software must be 5.2.3 or higher.
Keys are located in ./ssh . An encrypted version of the SSH private key is
stored on your machine protected by the YubiKey's hardware token. How to
configure SSH with YubiKey Security Keys U2F Authentication on Ubuntu [link to
cryptsus.com/blog](https://cryptsus.com/blog/how-to-configure-openssh-with-yubikey-security-keys-u2f-otp-authentication-ed25519-sk-ecdsa-sk-on-ubuntu-18.04.html)

- Check ssh version `ssh -V`
- Check YubiKey firmware version `lsusb -v 2>/dev/null | grep -A2 Yubico | grep
  "bcdDevice" | awk '{print $2}'`
- Generate a public/private key pair: `ssh-keygen -t ed25519-sk -C
  "$(hostname)-$(date
  +'%d-%m-%Y')-replace_this_text_with_your_physical_yubikey_number_for_better_identification"` 
- (If you have an older YubiKey or a key which does not support FIDO2, generate
  a public/private key pair: `ssh-keygen -t ed25519 -C "$(hostname)-$(date
  +'%d-%m-%Y')-replace_this_text_with_text_for_better_identification"`)
- Transfer the key to the ssh server: `ssh-copy-id -i ~/.ssh/id_ed25519_sk.pub
  server`
- To delete, remove the key from server ./ssh/authorized_keys

### To completely reset the YubiKey

Article `Resetting Your YubiKey 5 Series to Factory Defaults` on
[support.yubico.com](https://support.yubico.com/hc/en-us/articles/360013757959-Resetting-Your-YubiKey-5-series-to-Factory-Defaults).
This is listing the following:

- OTP 	[Resetting the OTP Application on the YubiKey](https://support.yubico.com/support/solutions/articles/15000008846-resetting-the-otp-applet-on-the-yubikey)
- FIDO U2F / FIDO 2 	[Resetting the FIDO-U2F / FIDO2 Application on Your Yubikey](https://support.yubico.com/support/solutions/articles/15000016440-resetting-the-fido2-application-on-your-yubikey-or-security-key)
- PIV (Smart Card) 	[Resetting the Smart Card (PIV) Application on Your YubiKey](https://support.yubico.com/support/solutions/articles/15000008587-resetting-the-smart-card-piv-applet-on-your-yubikey)
- OATH 	[Resetting the OATH Application on the YubiKey](https://support.yubico.com/support/solutions/articles/15000008759-resetting-the-oath-applet-on-the-yubikey)
- OpenPGP 	[Resetting the OpenPGP Application on Your YubiKey](https://support.yubico.com/support/solutions/articles/15000006421-resetting-the-openpgp-applet-on-your-yubikey) (Not available on the YubiKey 5 FIPS Series)

## Untested alternatives

- SoloKeys [link to solokeys.com ](https://solokeys.com/collections/all)
- GoTrust Idem Key [link to CZC.cz](https://www.czc.cz/gotrust-idem-key-usb-nfc-bezpecnostni-klic/306894/produkt), [link to gotrustid.com](https://www.gotrustid.com/idem-key)






